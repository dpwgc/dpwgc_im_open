package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.Notice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 消息通知相关操作
 */
@Repository
@Mapper
public interface NoticeMapper extends BaseMapper<Notice> {

    /**
     * 根据群组id返回群组通知列表
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    List<Notice> queryNoticeByGroupId(@Param("groupId") int groupId,
                                      @Param("startPage") Integer startPage,
                                      @Param("endPage") Integer endPage);
}
