package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户相关操作
 */
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 根据用户手机号码查找用户
     * @param phone
     * @return
     */
    User queryUserByPhone(String phone);
}
