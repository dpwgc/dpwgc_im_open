package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.UserGroup;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 用户与群组的关系
 */
@Repository
@Mapper
public interface UserGroupMapper extends BaseMapper<UserGroup> {

    /**
     * 检查用户是否加入该群聊
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    UserGroup checkUserGroup(int userId,int groupId);

    /**
     * 用户加入群聊
     * @param userGroup 用户与群组的关联信息
     * @return
     */
    int userJoinGroup(UserGroup userGroup);

    /**
     * 用户退出群聊
     * @param userGroup 用户与群组的关联信息
     * @return
     */
    int userExitGroup(UserGroup userGroup);

    /**
     * 更新用户已读消息数量
     * @param userId
     * @param groupId
     * @return
     */
    int updateLookNum(int userId,int groupId,int lookNum);
}
