package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.AppVer;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Mapper
public interface AppVerMapper extends BaseMapper<AppVer> {

    /**
     * 获取APP最新版本信息
     * @return
     */
    AppVer queryNewAppVer();

    /**
     * 获取APP全部版本信息
     * @return
     */
    List<AppVer> queryAllAppVer();
}
