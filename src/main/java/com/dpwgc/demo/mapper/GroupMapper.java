package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.Admin;
import com.dpwgc.demo.model.Group;
import com.dpwgc.demo.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 群组相关操作
 */
@Repository
@Mapper
public interface GroupMapper extends BaseMapper<Group> {

    /**
     * 根据用户id返回用户加入的群组列表
     * @param userId 用户id
     * @return
     */
    List<Group> queryGroupByUserId(@Param("userId") int userId);

    /**
     * 根据关键字搜索群组
     * @param keyword 关键词
     * @return
     */
    List<Group> queryGroupByKeyword(@Param("keyword") String keyword,
                                    @Param("startPage") Integer startPage,
                                    @Param("endPage") Integer endPage);

    /**
     * 根据关键字搜索用户已加入的群组
     * @param userId 关键字
     * @param keyword 关键字
     * @return
     */
    List<Group> queryUserGroupByKeyword(@Param("userId") int userId,
                                        @Param("keyword") String keyword);

    /**
     * 根据群组id获取群组用户列表
     * @param groupId 群组id
     * @return
     */
    List<User> queryUserByGroupId(@Param("groupId") int groupId);

    /**
     * 根据群组id获取群组管理员列表
     * @param groupId 群组id
     * @return
     */
    List<Admin> queryAdminByGroupId(@Param("groupId") int groupId);

    /**
     * 根据群组id获取群组群主
     * @param groupId 群组id
     * @return
     */
    Admin querySuperAdminByGroupId(@Param("groupId") int groupId);

    /**
     * 根据群组id获取申请加入群组的用户列表
     * @param groupId 群组id
     * @return
     */
    List<User> queryUserApplyByGroupId(@Param("groupId") int groupId);

    /**
     * 根据群组id查询群组
     * @param id 群组id
     * @return
     */
    Group queryGroupById(@Param("id") int id);

    /**
     * 根据群组id更新群组
     * @param group 群组
     * @return
     */
    int updateGroupById(Group group);

    /**
     * 插入新群组
     * @param group 群组
     * @return
     */
    int insertGroup(Group group);

}
