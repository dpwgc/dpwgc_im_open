package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 聊天消息相关操作
 */
@Repository
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 根据群聊id返回群聊信息
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    List<Message> queryMessageByGroupId(@Param("groupId") int groupId,
                                        @Param("startPage") Integer startPage,
                                        @Param("endPage") Integer endPage);

    /**
     * 根据消息id删除该消息
     * @param id 消息id
     * @return
     */
    int deleteMessage(int id);
}
