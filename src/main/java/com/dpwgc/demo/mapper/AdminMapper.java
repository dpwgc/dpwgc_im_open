package com.dpwgc.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.dpwgc.demo.model.Admin;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * 管理员相关操作
 */
@Repository
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

    /**
     * 判断用户是否是该群管理员 & 检查管理员等级
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    Admin checkAdminLv(int userId,int groupId);

    /**
     * 群主撤销管理员权限
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    int deleteAdmin(int userId,int groupId);

    /**
     * 更新管理员等级
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    int updateAdminLv(int userId,int groupId,int lv);


}
