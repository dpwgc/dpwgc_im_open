package com.dpwgc.demo.utils;

import com.alibaba.fastjson.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Json数据解析
 */
public class JsonReadUtil {

    public static Map getMap(String json){
        Map map = new LinkedHashMap();
        try {
            //转换为Map对象
            map = JSONObject.parseObject(json, LinkedHashMap.class);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

}
