package com.dpwgc.demo.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import javax.mail.internet.MimeMessage;

import java.io.File;

/**
 * 邮件发送工具
 */
public class MailUtil {

    @Autowired
    JavaMailSenderImpl javaMailSender;

    //邮件投送(传入参数：目标邮件)
    public void mailOut(String mail) throws javax.mail.MessagingException{

        DateUtil dateUtil = new DateUtil();
        String time = dateUtil.getDateTime();

        //获取文件名后缀
        //String substring = name.substring(name.lastIndexOf("."));

        //1、创建一个复杂的邮件
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        //邮件主题
        helper.setSubject("主题");
        //邮件内容
        helper.setText("<h1>你好</h1><br><h1>hello</h1><br><h3>"+time+"</h3><br><h3>@dpwgc</h3>",true);
        helper.setTo(mail);
        helper.setFrom("xxxxx@163.com");//主机邮箱地址
        //附件添加压缩文件
        //helper.addAttachment("xxx.jar",new File("xxx\\xxx\\xxx"));

        javaMailSender.send(mimeMessage);
    }
}
