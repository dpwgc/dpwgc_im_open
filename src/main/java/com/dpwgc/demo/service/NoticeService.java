package com.dpwgc.demo.service;

import com.dpwgc.demo.mapper.NoticeMapper;
import com.dpwgc.demo.model.Notice;
import com.dpwgc.demo.utils.DateUtil;
import com.dpwgc.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeService {

    @Autowired
    NoticeMapper noticeMapper;
    @Autowired
    DateUtil dateUtil;
    /**
     * 插入通知
     * @param userId 要通知的用户id
     * @param groupId 群组id
     * @param noticeText 通知文本
     * @param noticeType 通知类型
     * @return
     */
    public ResultUtil<Object> insertNoticeService(int userId,int groupId,String noticeText,int noticeType){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        if(noticeText.length()<1 || noticeText.length()>50){

            resultUtil.setCode(100);
            resultUtil.setMsg("通知格式违规");
            resultUtil.setData("");
            return resultUtil;
        }

        Notice notice = new Notice();
        notice.setCreateTime(dateUtil.getDateTime());
        notice.setGroupId(groupId);
        notice.setNoticeText(noticeText);
        notice.setNoticeType(noticeType);
        notice.setStatus(1);

        try{
            int i = noticeMapper.insert(notice);

            resultUtil.setCode(200);
            resultUtil.setMsg("操作成功");
            resultUtil.setData(i);
            return resultUtil;

        }catch (Exception e){

            resultUtil.setCode(100);
            resultUtil.setMsg("操作失败");
            resultUtil.setData(e);
            return resultUtil;
        }
    }

    /**
     * 根据群组id返回群组通知列表
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    public ResultUtil<Object> queryNoticeByGroupIdService(int groupId,Integer startPage,Integer endPage){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        try{
            List<Notice> notices = noticeMapper.queryNoticeByGroupId(groupId,startPage,endPage);

            resultUtil.setCode(200);
            resultUtil.setMsg("操作成功");
            resultUtil.setData(notices);
            return resultUtil;

        }catch (Exception e){

            resultUtil.setCode(100);
            resultUtil.setMsg("操作失败");
            resultUtil.setData(e);
            return resultUtil;
        }
    }

    /**
     * 删除通知
     * @param noticeId 通知id
     * @return
     */
    public ResultUtil<Object> deleteNoticeService(int noticeId){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        try{

            Notice notice = noticeMapper.selectById(noticeId);
            notice.setStatus(0);
            int i = noticeMapper.updateById(notice);

            resultUtil.setCode(200);
            resultUtil.setMsg("操作成功");
            resultUtil.setData(i);
            return resultUtil;

        }catch (Exception e){

            resultUtil.setCode(100);
            resultUtil.setMsg("操作失败");
            resultUtil.setData(e);
            return resultUtil;
        }
    }
}
