package com.dpwgc.demo.service;

import com.dpwgc.demo.mapper.AppVerMapper;
import com.dpwgc.demo.model.AppVer;
import com.dpwgc.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppVerService {

    @Autowired
    AppVerMapper appVerMapper;

    /**
     * 获取APP最新版本信息
     * @return
     */
    public ResultUtil<Object> queryNewAppVerService(){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        AppVer appVer = appVerMapper.queryNewAppVer();

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(appVer);
        return resultUtil;
    }

    /**
     * 获取APP全部版本信息
     * @return
     */
    public ResultUtil<Object> queryAllAppVerService(){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        List<AppVer> appVers = appVerMapper.queryAllAppVer();

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(appVers);
        return resultUtil;
    }
}
