package com.dpwgc.demo.service;

import com.dpwgc.demo.mapper.GroupMapper;
import com.dpwgc.demo.mapper.MessageMapper;
import com.dpwgc.demo.mapper.UserMapper;
import com.dpwgc.demo.model.Group;
import com.dpwgc.demo.model.Message;
import com.dpwgc.demo.model.User;
import com.dpwgc.demo.utils.DateUtil;
import com.dpwgc.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * 聊天消息相关操作
 */
@Service
public class MessageService {

    @Autowired
    MessageMapper messageMapper;

    @Autowired
    GroupMapper groupMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    DateUtil dateUtil;

    /**
     * 根据群组id返回该群组的最新消息列表
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    public ResultUtil<Object> queryMessageByGroupIdService(int groupId,Integer startPage,Integer endPage){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        List<Message> messages = messageMapper.queryMessageByGroupId(groupId, startPage, endPage);

        //列表倒序
        Collections.reverse(messages);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(messages);
        return resultUtil;
    }

    /**
     * 插入消息
     * @param userId 用户id
     * @param groupId 群组id
     * @param messageData 消息主体
     * @return
     */
    public ResultUtil<Object> insertMessageService(int userId,int groupId,String messageData){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        if(messageData.length()>=999){
            resultUtil.setCode(100);
            resultUtil.setMsg("超过字数限制");
            resultUtil.setData("");
            return resultUtil;
        }

        Message message = new Message();

        message.setUserId(userId);
        message.setGroupId(groupId);
        message.setMessageData(messageData);
        message.setCreateTime(dateUtil.getDateTime());
        message.setUpdateTime(dateUtil.getDateTime());
        message.setStatus(1);

        int i = messageMapper.insert(message);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(i);
        return resultUtil;
    }

    /**
     * 根据消息id删除该消息
     * @param id 消息id
     * @return
     */
    public ResultUtil<Object> deleteMessageService(int id,int userId,int groupId){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        int i = messageMapper.deleteMessage(id);
        if(i == 0){
            resultUtil.setCode(100);
            resultUtil.setMsg("操作失败");
            resultUtil.setData(i);
            return resultUtil;
        }

        //更新群组信息
        Group group = groupMapper.queryGroupById(groupId);
        User user = userMapper.selectById(userId);

        String m = user.getNickname()+"撤回了一条消息";

        if(m.length()>16){
            group.setGroupText(m.substring(0, 15)+"...");
        }
        else{
            group.setGroupText(m);
        }
        group.setUpdateTime(dateUtil.getDateTime());
        group.setChatNum(group.getChatNum());
        int i1 = groupMapper.updateGroupById(group);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(i);
        return resultUtil;
    }
}
