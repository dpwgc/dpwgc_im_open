package com.dpwgc.demo.service;

import com.dpwgc.demo.config.TencentCosConfig;
import com.dpwgc.demo.utils.HttpUtil;
import com.dpwgc.demo.utils.ResultUtil;
import com.dpwgc.demo.utils.UploadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

/**
 * 腾讯云对象存储-文件上传
 */
@EnableConfigurationProperties(TencentCosConfig.class)
@Service
public class UploadService {

    @Autowired
    UploadUtil uploadUtil;
    @Autowired
    HttpUtil httpUtil;
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 文件上传（默认文件保存方式，按时间保存）
     * @param file 文件流
     * @param type 文件类型
     * @param session 会话
     * @return
     */
    public ResultUtil<Object> uploadFileService(MultipartFile file, String type, HttpSession session){

        return uploadUtil.uploadFileUtil(file, type, session);
    }

    /**
     * 文件上传(自定义文件名与文件路径)
     * @param file 文件流
     * @param type 文件类型
     * @param fileName 自定义文件名称
     * @param url 指定存储地址
     * @param session 会话
     * @return
     */
    public ResultUtil<Object> uploadFileFreeService(MultipartFile file, String type, String fileName, String url, HttpSession session){

        return uploadUtil.uploadFileFreeUtil(file, type, fileName,url,session);
    }
}
