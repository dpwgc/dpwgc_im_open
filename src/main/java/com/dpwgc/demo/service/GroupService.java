package com.dpwgc.demo.service;

import com.dpwgc.demo.mapper.AdminMapper;
import com.dpwgc.demo.mapper.GroupMapper;
import com.dpwgc.demo.mapper.UserGroupMapper;
import com.dpwgc.demo.model.Admin;
import com.dpwgc.demo.model.Group;
import com.dpwgc.demo.model.User;
import com.dpwgc.demo.model.UserGroup;
import com.dpwgc.demo.utils.DateUtil;
import com.dpwgc.demo.utils.ResultUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.List;

/**
 * 群组相关操作
 */
@Service
public class GroupService {

    @Autowired
    GroupMapper groupMapper;

    @Autowired
    AdminMapper adminMapper;

    @Autowired
    UserGroupMapper userGroupMapper;

    @Autowired
    DateUtil dateUtil;

    /**
     * 根据用户id获取用户群组列表
     * @param userId 用户id
     * @return
     */
    public ResultUtil<Object> queryGroupByUserIdService(int userId){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        List<Group> groups = groupMapper.queryGroupByUserId(userId);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(groups);
        return resultUtil;
    }

    /**
     * 新建群组
     * @param userId 用户id
     * @param groupName 群组名称
     * @param groupImg 群组头像
     * @return
     */
    public ResultUtil<Object> insertGroupService(int userId,String groupName,String groupImg){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        if(groupName.length()<1 || groupName.length()>20){
            resultUtil.setCode(100);
            resultUtil.setMsg("群名格式违规");
            resultUtil.setData("");
            return resultUtil;
        }

        //开启事务
        try{
            //新建群聊信息
            Group group = new Group();
            group.setGroupName(groupName);
            group.setGroupImg(groupImg);
            group.setCreateTime(dateUtil.getDateTime());
            group.setUpdateTime(dateUtil.getDateTime());
            group.setStatus(1);
            group.setChatNum(0);
            //插入群聊信息
            int i1 = groupMapper.insertGroup(group);

            //新建用户信息
            UserGroup userGroup = new UserGroup();
            userGroup.setUserId(userId);
            userGroup.setGroupId(group.getId());
            userGroup.setCreateTime(dateUtil.getDateTime());
            userGroup.setUpdateTime(dateUtil.getDateTime());
            userGroup.setLookNum(0);
            userGroup.setStatus(1);
            //插入用户信息
            int i2 = userGroupMapper.insert(userGroup);

            //新建管理员信息
            Admin admin = new Admin();
            admin.setUserId(userId);
            admin.setGroupId(group.getId());
            admin.setLv(1);
            admin.setCreateTime(dateUtil.getDateTime());
            admin.setUpdateTime(dateUtil.getDateTime());
            admin.setStatus(1);
            //插入管理员信息
            int i3 = adminMapper.insert(admin);

        } catch (Exception e) {
            e.printStackTrace();
            //设置手动回滚
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();

            resultUtil.setCode(100);
            resultUtil.setMsg("操作失败");
            resultUtil.setData("");
            return resultUtil;
        }

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData("");
        return resultUtil;
    }

    /**
     * 根据关键字搜索群组
     * @param keyword 关键字
     * @return
     */
    public ResultUtil<Object> queryGroupByKeywordService(String keyword,Integer startPage,Integer endPage){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        if(keyword.length()>20){
            resultUtil.setCode(100);
            resultUtil.setMsg("关键词过长");
            resultUtil.setData("");
            return resultUtil;
        }

        List<Group> groups = groupMapper.queryGroupByKeyword(keyword,startPage,endPage);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(groups);
        return resultUtil;
    }

    /**
     * 根据关键字搜索用户已加入的群组
     * @param userId 关键字
     * @param keyword 关键字
     * @return
     */
    public ResultUtil<Object> queryUserGroupByKeywordService(int userId,String keyword){

        ResultUtil<Object> resultUtil = new ResultUtil<>();

        if(keyword.length()>20){
            resultUtil.setCode(100);
            resultUtil.setMsg("关键词过长");
            resultUtil.setData("");
            return resultUtil;
        }

        List<Group> groups = groupMapper.queryUserGroupByKeyword(userId,keyword);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(groups);
        return resultUtil;
    }

    /**
     * 根据群组id获取群组用户列表
     * @param groupId 群组id
     * @return
     */
    public ResultUtil<Object> queryUserByGroupIdService(int groupId){
        ResultUtil<Object> resultUtil = new ResultUtil<>();

        List<User> users = groupMapper.queryUserByGroupId(groupId);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(users);
        return resultUtil;
    }

    /**
     * 根据群组id获取群组管理员列表
     * @param groupId 群组id
     * @return
     */
    public ResultUtil<Object> queryAdminByGroupIdService(int groupId){
        ResultUtil<Object> resultUtil = new ResultUtil<>();

        List<Admin> admins = groupMapper.queryAdminByGroupId(groupId);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(admins);
        return resultUtil;
    }

    /**
     * 根据群组id获取群组群主
     * @param groupId 群组id
     * @return
     */
    public ResultUtil<Object> querySuperAdminByGroupIdService(int groupId){
        ResultUtil<Object> resultUtil = new ResultUtil<>();

        Admin admin = groupMapper.querySuperAdminByGroupId(groupId);

        resultUtil.setCode(200);
        resultUtil.setMsg("操作成功");
        resultUtil.setData(admin);
        return resultUtil;
    }
}
