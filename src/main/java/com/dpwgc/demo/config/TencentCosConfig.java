package com.dpwgc.demo.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


/**
 * 腾讯对象存储配置
 */
@Configuration
@ConfigurationProperties(prefix = "tencent", ignoreUnknownFields = false)
@Component
public class TencentCosConfig {


    private String accessKey;
    private String secretKey;
    private String bucket;
    private String bucketName;
    private String path;
    private String qianzui;
    private String cdnurl;

    @Bean
    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    @Bean
    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Bean
    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    @Bean
    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    @Bean
    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Bean
    public String getQianzui() {
        return qianzui;
    }

    public void setQianzui(String qianzui) {
        this.qianzui = qianzui;
    }

    @Bean
    public String getCdnurl() {
        return cdnurl;
    }

    public void setCdnurl(String cdnurl) {
        this.cdnurl = cdnurl;
    }
}
