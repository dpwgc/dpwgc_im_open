package com.dpwgc.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 邮件发送配置
 */
@Component
@ConfigurationProperties(prefix = "mail")
public class MailConfig {

    private String from;

    private String to;

}

