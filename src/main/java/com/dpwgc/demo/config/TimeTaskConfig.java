package com.dpwgc.demo.config;

import com.dpwgc.demo.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessagingException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.IOException;

/**
 * 定时任务
 */
@Configuration//兼备Component的效果
@EnableScheduling//定时任务开启
@EnableAsync//多线程开启
public class TimeTaskConfig {

    //添加定时任务
    //@Scheduled(cron = "0 0/5 * * * ?")
    @Scheduled(cron = "0 0 21 ? * SAT")

    //执行操作
    private void configureTasks() throws MessagingException, IOException {

        DateUtil dateUtil = new DateUtil();
        System.err.println("定时任务执行时间:" +dateUtil.getDateTime());
    }
}



