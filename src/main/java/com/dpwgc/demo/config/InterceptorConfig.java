package com.dpwgc.demo.config;

import com.dpwgc.demo.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * 注册拦截器
 */
@Configuration
public class InterceptorConfig extends WebMvcConfigurerAdapter
{
    /**
     * 拦截器是在Bean实例化前注入的，注入拦截器前Redis并没有被实例化
     * 需要手动实例化RedisTemplate
     * @return
     */
    @Bean
    public LoginInterceptor getLoginInterceptor(){
        return new LoginInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(getLoginInterceptor());
        registration.addPathPatterns("/**");                      //所有路径都被拦截
        registration.excludePathPatterns(                         //添加不拦截路径
                "/User/login",
                "/User/insertUser",
                "/Upload/**",
                "/swagger*/**",
                "/v2/**",
                "/webjars/**",
                "/Test/**"
        );
    }
}