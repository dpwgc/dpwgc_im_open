package com.dpwgc.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Swagger接口文档配置
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket getUserDocket(){
        //可以添加多个header或参数
        ParameterBuilder id = new ParameterBuilder();
        id.parameterType("header") //参数类型支持header, cookie, body, query etc
                .name("id") //参数名
                .defaultValue("") //默认值
                .description("用户id")
                .modelRef(new ModelRef("string"))//指定参数值的类型
                .required(false).build(); //非必需，这里是全局配置

        ParameterBuilder token = new ParameterBuilder();
        token.parameterType("header") //参数类型支持header, cookie, body, query etc
                .name("token") //参数名
                .defaultValue("") //默认值
                .description("令牌")
                .modelRef(new ModelRef("string"))//指定参数值的类型
                .required(false).build(); //非必需，这里是全局配置

        List<Parameter> parameters = new ArrayList<Parameter>();
        parameters.add(id.build());
        parameters.add(token.build());

        ApiInfo apiInfo=new ApiInfoBuilder()
                .title("[dpwgc_im]基于Spring Boot整合WebSocket的IM系统后端")//api标题
                .description("项目相关接口描述")//api描述
                .version("1.0.0")//版本号
                .contact("dpwgc")//本API负责人的联系信息
                .build();
        return new Docket(DocumentationType.SWAGGER_2)//文档类型（swagger2）
                .apiInfo(apiInfo)//设置包含在json ResourceListing响应中的api元信息
                .select()//启动用于api选择的构建器
                .apis(RequestHandlerSelectors.basePackage("com.dpwgc.demo.controller"))//扫描接口的包
                .paths(PathSelectors.any())//路径过滤器（扫描所有路径）
                .build()
                .globalOperationParameters(parameters);
    }
}
