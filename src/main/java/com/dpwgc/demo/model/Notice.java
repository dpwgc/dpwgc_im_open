package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 通知类
 */
@TableName(value = "im_notice")
public class Notice {

    @TableId(value = "id",type = IdType.AUTO)
    private int id; //通知编号
    private int groupId; //群组id
    private String noticeText; //通知文本
    private int noticeType; //通知类型：1系统通知 2群组通知
    private String createTime; //通知时间
    private int status; //通知状态：0未读 1已读 3删除

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getNoticeText() {
        return noticeText;
    }

    public void setNoticeText(String noticeText) {
        this.noticeText = noticeText;
    }

    public int getNoticeType() {
        return noticeType;
    }

    public void setNoticeType(int noticeType) {
        this.noticeType = noticeType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Notice{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", noticeText='" + noticeText + '\'' +
                ", noticeType=" + noticeType +
                ", createTime='" + createTime + '\'' +
                ", status=" + status +
                '}';
    }
}
