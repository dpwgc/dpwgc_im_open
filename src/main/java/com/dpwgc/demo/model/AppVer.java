package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * APP当前最新版本信息
 */
@TableName(value = "im_app_ver")
public class AppVer {

    @TableId(value = "id",type = IdType.AUTO)
    private int id; //id
    private String appVer; //app版本号
    private String verContent; //版本更新内容
    private String url; //更新包url
    private String createTime; //版本创建时间

    public void setId(int id) {
        this.id = id;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setAppVer(String appVer) {
        this.appVer = appVer;
    }

    public void setVerContent(String verContent) {
        this.verContent = verContent;
    }

    public String getUrl() {
        return url;
    }

    public Integer getId() {
        return id;
    }

    public String getAppVer() {
        return appVer;
    }

    public String getVerContent() {
        return verContent;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    @Override
    public String toString() {
        return "AppVer{" +
                "id=" + id +
                ", appVer='" + appVer + '\'' +
                ", verContent='" + verContent + '\'' +
                ", url='" + url + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
