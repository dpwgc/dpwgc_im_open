package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 用户与群组的关系
 */
@TableName(value = "im_user_group")
public class UserGroup {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;//自增id
    private int userId;//用户id
    private int groupId;//群组id
    private String createTime;//关系创建时间
    private String updateTime;//关系最近关系时间
    private int status;//关系当前状态：1已加入群组 2已申请待批准 3已删除
    private int lookNum;//已读消息数量

    public void setLookNum(int lookNum) {
        this.lookNum = lookNum;
    }

    public int getLookNum() {
        return lookNum;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "UserGroup{" +
                "id=" + id +
                ", userId=" + userId +
                ", groupId=" + groupId +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", status=" + status +
                ", lookNum=" + lookNum +
                '}';
    }
}
