package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 群聊消息
 */
@TableName(value = "im_message")
public class Message {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;//消息id
    private int userId;//用户id
    private int groupId;//群组id
    private String messageData;//消息主体内容
    private String createTime;//消息创建时间
    private String updateTime;//消息最近更新时间
    private int status;//消息状态：1正常 2撤回 3删除

    /**
     * 用户表字段
     */
    private String nickname;//用户昵称
    private String headImg;//用户头像

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getHeadImg() {
        return headImg;
    }

    public String getNickname() {
        return nickname;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setMessageData(String messageData) {
        this.messageData = messageData;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public int getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getMessageData() {
        return messageData;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", userId=" + userId +
                ", groupId=" + groupId +
                ", messageData='" + messageData + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", status=" + status +
                ", nickname='" + nickname + '\'' +
                ", headImg='" + headImg + '\'' +
                '}';
    }
}
