package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 访问日志记录
 */
@TableName(value = "im_access_log")
public class AccessLog {

    @TableId(value = "id",type = IdType.AUTO)
    private int id; //日志编号
    private int userId; //访问者的用户id
    private String ip; //访问者IP记录
    private String uri; //访问者请求的控制器路径记录
    private String logTime; //日志记录时间

    public void setId(int id) {
        this.id = id;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public int getId() {
        return id;
    }

    public int getUserId() {
        return userId;
    }

    public String getIp() {
        return ip;
    }

    public String getUri() {
        return uri;
    }

    public String getLogTime() {
        return logTime;
    }

    @Override
    public String toString() {
        return "AccessLog{" +
                "id=" + id +
                ", userId=" + userId +
                ", ip='" + ip + '\'' +
                ", uri='" + uri + '\'' +
                ", logTime='" + logTime + '\'' +
                '}';
    }
}
