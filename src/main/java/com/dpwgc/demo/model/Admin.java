package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 管理员
 */
@TableName(value = "im_admin")
public class Admin {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;//管理员id
    private int userId;//管理员的用户id
    private int groupId;//管理员所在群组id
    private int lv;//管理员等级：1群主 2管理员
    private int status;//管理员状态：1正常 2封禁 3删除
    private String createTime;//管理员创建时间
    private String updateTime;//管理员最近更新时间

    /**
     * 用户表字段
     */
    private String nickname;//用户昵称
    private String headImg;//用户头像

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setLv(int lv) {
        this.lv = lv;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getCreateTime() {
        return createTime;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getUserId() {
        return userId;
    }

    public int getLv() {
        return lv;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public String getHeadImg() {
        return headImg;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "id=" + id +
                ", userId=" + userId +
                ", groupId=" + groupId +
                ", lv=" + lv +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", nickname='" + nickname + '\'' +
                ", headImg='" + headImg + '\'' +
                '}';
    }
}
