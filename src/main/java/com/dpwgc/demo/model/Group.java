package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 群组
 */
@TableName(value = "im_group")
public class Group {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;//群组id
    private String groupName;//群组名称
    private String groupImg;//群组头像
    private String groupText;//群组最新消息
    private String createTime;//群组创建时间
    private String updateTime;//群组最近更新时间
    private int status;//群组状态：1正常 2封禁 3删除
    private int chatNum;//群聊消息总数

    /**
     * 用户群聊关联表字段
     */
    private int lookNum;//用户已读消息数量

    public void setLookNum(int lookNum) {
        this.lookNum = lookNum;
    }

    public int getLookNum() {
        return lookNum;
    }

    public void setChatNum(int chatNum) {
        this.chatNum = chatNum;
    }

    public int getChatNum() {
        return chatNum;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setGroupImg(String groupImg) {
        this.groupImg = groupImg;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setGroupText(String groupText) {
        this.groupText = groupText;
    }

    public int getStatus() {
        return status;
    }

    public int getId() {
        return id;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getGroupImg() {
        return groupImg;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getGroupText() {
        return groupText;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", groupName='" + groupName + '\'' +
                ", groupImg='" + groupImg + '\'' +
                ", groupText='" + groupText + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", status=" + status +
                ", chatNum=" + chatNum +
                ", lookNum=" + lookNum +
                '}';
    }
}
