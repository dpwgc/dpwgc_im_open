package com.dpwgc.demo.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 用户
 */
@TableName(value = "im_user")
public class User {

    @TableId(value = "id",type = IdType.AUTO)
    private int id;//用户id
    private String phone;//用户电话号码
    private String nickname;//用户昵称
    private String password;//用户密码
    private String createTime;//用户创建时间
    private String updateTime;//用户最近更新时间
    private String headImg;//用户头像
    private int status;//用户状态：1正常 2封禁 3删除

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public String getNickname() {
        return nickname;
    }

    public int getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public String getPassword() {
        return password;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", nickname='" + nickname + '\'' +
                ", password='" + password + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", headImg='" + headImg + '\'' +
                ", status=" + status +
                '}';
    }
}
