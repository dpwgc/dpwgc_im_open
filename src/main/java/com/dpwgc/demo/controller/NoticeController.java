package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.NoticeService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Api("消息通知相关api")
@RestController
@RequestMapping(value = "/Notice")
public class NoticeController {

    @Autowired
    NoticeService noticeService;

    /**
     * 插入通知
     * @param userId 要通知的用户id
     * @param groupId 群组id
     * @param noticeText 通知文本
     * @param noticeType 通知类型
     * @return
     */
    @ApiOperation("插入通知")
    @RequestMapping(value = "/insertNotice",method = RequestMethod.POST)
    public ResultUtil<Object> insertNotice(@RequestParam("userId") int userId,
                                           @RequestParam("groupId") int groupId,
                                           @RequestParam("noticeText") String noticeText,
                                           @RequestParam("noticeType") int noticeType){

        return noticeService.insertNoticeService(userId, groupId, noticeText, noticeType);
    }

    /**
     * 根据群组id返回群组通知列表
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    @ApiOperation("根据群组id返回群组通知列表")
    @RequestMapping(value = "/queryNoticeByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> queryNoticeByGroupId(@RequestParam("groupId") int groupId,
                                                   @RequestParam("startPage") Integer startPage,
                                                   @RequestParam("endPage") Integer endPage){

        return noticeService.queryNoticeByGroupIdService(groupId,startPage,endPage);
    }

    /**
     * 删除通知
     * @param noticeId 通知id
     * @return
     */
    @ApiOperation("删除通知")
    @RequestMapping(value = "/deleteNotice",method = RequestMethod.POST)
    public ResultUtil<Object> deleteNotice(@RequestParam("noticeId") int noticeId){

        return noticeService.deleteNoticeService(noticeId);
    }

}
