package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.UserService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("用户相关api")
@RestController
@RequestMapping(value = "/User")
public class UserController {

    @Autowired
    UserService userService;

    /**
     * 登录
     * @param phone 用户手机号码
     * @param password 用户密码
     * @return
     */
    @ApiOperation("登录")
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public ResultUtil<Object> login(@RequestParam("phone") String phone,
                                    @RequestParam("password") String password){

        return userService.loginService(phone,password);
    }

    /**
     * 注册
     * @param phone 用户手机号码
     * @param nickname 用户昵称
     * @param password 用户密码
     * @param headImg 用户头像
     * @return
     */
    @ApiOperation("注册")
    @RequestMapping(value = "/insertUser",method = RequestMethod.POST)
    public ResultUtil<Object> insertUser(@RequestParam("phone") String phone,
                                         @RequestParam("nickname") String nickname,
                                         @RequestParam("password") String password,
                                         @RequestParam("headImg") String headImg){

        return userService.insertUserService(phone,nickname, password,headImg);
    }

    /**
     * 根据用户id返回用户信息
     * @param id 用户id
     * @return
     */
    @ApiOperation("根据用户id返回用户信息")
    @RequestMapping(value = "/queryUserById",method = RequestMethod.POST)
    public ResultUtil<Object> queryUserById(@RequestParam("id") int id){

        return userService.queryUserByIdService(id);
    }

    /**
     * 根据用户id修改用户个人信息
     * @param id
     * @return
     */
    @ApiOperation("用户修改个人信息")
    @RequestMapping(value = "/updateUserById",method = RequestMethod.POST)
    public ResultUtil<Object> updateUserById(@RequestParam("id") int id,
                                             @RequestParam("phone") String phone,
                                             @RequestParam("nickname") String nickname,
                                             @RequestParam("headImg") String headImg){

        return userService.updateUserByIdService(id, phone, nickname, headImg);
    }

    /**
     * 用户申请加入群组
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("用户申请加入群组")
    @RequestMapping(value = "/userApplyGroup",method = RequestMethod.POST)
    public ResultUtil<Object> userApplyGroup(@RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId){

        return userService.userApplyGroupService(userId,groupId);
    }

    /**
     * 用户退出群组
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("用户退出群组")
    @RequestMapping(value = "/userExitGroup",method = RequestMethod.POST)
    public ResultUtil<Object> userExitGroup(@RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId){

        return userService.userExitGroupService(userId,groupId);
    }
}
