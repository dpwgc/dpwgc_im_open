package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.MessageService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("群聊消息相关api")
@RestController
@RequestMapping(value = "/Message")
public class MessageController {

    @Autowired
    MessageService messageService;

    /**
     * 根据群组id返回该群组的最新消息列表
     * @param groupId 群组id
     * @param startPage 起始页
     * @param endPage 从起始页开始计数——计到第endPage页结束
     * @return
     */
    @ApiOperation("根据群组id返回该群组的最新消息列表")
    @RequestMapping(value = "/queryMessageByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> queryMessageByGroupId(@RequestParam("groupId") int groupId,
                                                    @RequestParam("startPage") Integer startPage,
                                                    @RequestParam("endPage") Integer endPage) {

        return messageService.queryMessageByGroupIdService(groupId,startPage,endPage);
    }

    /**
     * 插入消息
     * @param userId 用户id
     * @param groupId 群组id
     * @param messageData 消息主体
     * @return
     */
    @ApiOperation("插入消息")
    @RequestMapping(value = "/insertMessage",method = RequestMethod.POST)
    public ResultUtil<Object> insertMessage(@RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId,
                                            @RequestParam("messageData") String messageData) {

        return messageService.insertMessageService(userId,groupId,messageData);
    }

    /**
     * 根据消息id删除该消息
     * @param id 消息id
     * @return
     */
    @ApiOperation("删除消息")
    @RequestMapping(value = "/deleteMessage",method = RequestMethod.POST)
    public ResultUtil<Object> deleteMessage(@RequestParam("id") int id,
                                            @RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId) {

        return messageService.deleteMessageService(id,userId,groupId);
    }
}
