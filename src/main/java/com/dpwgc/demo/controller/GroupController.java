package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.GroupService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@Api("群组相关api")
@RestController
@RequestMapping(value = "/Group")
public class GroupController {

    @Autowired
    GroupService groupService;

    /**
     * 根据用户id获取用户群组列表
     * @param userId 用户id
     * @return
     */
    @ApiOperation("根据用户id获取用户群组列表")
    @RequestMapping(value = "/queryGroupByUserId",method = RequestMethod.POST)
    public ResultUtil<Object> queryGroupByUserId(@RequestParam("userId") int userId){

        return groupService.queryGroupByUserIdService(userId);
    }

    /**
     * 新建群组
     * @param userId 创建该群组的用户id
     * @param groupName 群组名称
     * @param groupImg 群组头像
     * @return
     */
    @ApiOperation("新建群组")
    @RequestMapping(value = "/insertGroup",method = RequestMethod.POST)
    public ResultUtil<Object> insertGroup(@RequestParam("userId") int userId,
                                          @RequestParam("groupName") String groupName,
                                          @RequestParam("groupImg") String groupImg){

        return groupService.insertGroupService(userId,groupName, groupImg);
    }

    /**
     * 根据关键字搜索群组
     * @param keyword 关键字
     * @param startPage 起始页
     * @param endPage 每页长度
     * @return
     */
    @ApiOperation("根据关键字搜索群组")
    @RequestMapping(value = "/queryGroupByKeyword",method = RequestMethod.POST)
    public ResultUtil<Object> queryGroupByKeyword(@RequestParam("keyword") String keyword,
                                                  @RequestParam("startPage") Integer startPage,
                                                  @RequestParam("endPage") Integer endPage){

        return groupService.queryGroupByKeywordService(keyword,startPage,endPage);
    }

    /**
     * 根据关键字搜索用户已加入的群组
     * @param userId 用户id
     * @param keyword 关键字
     * @return
     */
    @ApiOperation("根据关键字搜索用户已加入的群组")
    @RequestMapping(value = "/queryUserGroupByKeyword",method = RequestMethod.POST)
    public ResultUtil<Object> queryUserGroupByKeyword(@RequestParam("userId") int userId,
                                                      @RequestParam("keyword") String keyword){

        return groupService.queryUserGroupByKeywordService(userId,keyword);
    }

    /**
     * 根据群组id获取群组用户列表
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("根据群组id获取群组用户列表")
    @RequestMapping(value = "/queryUserByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> queryUserByGroupId(@RequestParam("groupId")  int groupId){

        return groupService.queryUserByGroupIdService(groupId);
    }

    /**
     * 根据群组id获取群组管理员列表
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("根据群组id获取群组管理员列表")
    @RequestMapping(value = "/queryAdminByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> queryAdminByGroupId(@RequestParam("groupId")  int groupId){

        return groupService.queryAdminByGroupIdService(groupId);
    }

    /**
     * 根据群组id获取群组群主
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("根据群组id获取群组群主")
    @RequestMapping(value = "/querySuperAdminByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> querySuperAdminByGroupId(@RequestParam("groupId")  int groupId){

        return groupService.querySuperAdminByGroupIdService(groupId);
    }
}
