package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.AdminService;
import com.dpwgc.demo.service.UserService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api("管理员相关api")
@RestController
@RequestMapping(value = "/Admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @Autowired
    UserService userService;

    /**
     * 新建管理员
     * @param userId 用户id
     * @param groupId 群组id
     * @param lv 管理员等级
     * @return
     */
    @ApiOperation("新建管理员")
    @RequestMapping(value = "/insertAdmin",method = RequestMethod.POST)
    public ResultUtil<Object> insertAdmin(@RequestParam("userId") int userId,
                                          @RequestParam("groupId") int groupId,
                                          @RequestParam("lv") int lv){

        return adminService.insertAdminService(userId,groupId,lv);
    }

    /**
     * 根据群组id获取申请加入群组的用户列表
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("根据群组id获取申请加入群组的用户列表")
    @RequestMapping(value = "/queryUserApplyByGroupId",method = RequestMethod.POST)
    public ResultUtil<Object> queryUserApplyByGroupId(@RequestParam("groupId") int groupId){

        return adminService.queryUserApplyByGroupIdService(groupId);
    }

    /**
     * 批准用户加入群组
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("批准用户加入群组")
    @RequestMapping(value = "/userJoinGroup",method = RequestMethod.POST)
    public ResultUtil<Object> userJoinGroup(@RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId){

        return adminService.userJoinGroupService(userId, groupId);
    }

    /**
     * 拒绝用户加入群组
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("用户退出群组")
    @RequestMapping(value = "/userExitGroup",method = RequestMethod.POST)
    public ResultUtil<Object> userExitGroup(@RequestParam("userId") int userId,
                                            @RequestParam("groupId") int groupId){

        return userService.userExitGroupService(userId,groupId);
    }

    /**
     * 判断用户是否是该群管理员 & 检查管理员等级
     * @param userId 用户id
     * @param groupId 群组id
     * @return 返回管理员等级
     */
    @ApiOperation("判断用户是否是管理员 & 检查管理员等级")
    @RequestMapping(value = "/checkAdminLv",method = RequestMethod.POST)
    public ResultUtil<Object> checkAdminLv(@RequestParam("userId") int userId,
                                           @RequestParam("groupId") int groupId){

        return adminService.checkAdminLvService(userId, groupId);
    }

    /**
     * 群主撤销管理员权限
     * @param userId 用户id
     * @param groupId 群组id
     * @return
     */
    @ApiOperation("群主撤销管理员权限")
    @RequestMapping(value = "/deleteAdmin",method = RequestMethod.POST)
    public ResultUtil<Object> deleteAdmin(@RequestParam("userId") int userId,
                                          @RequestParam("groupId") int groupId){

        return adminService.deleteAdminService(userId, groupId);
    }

    /**
     * 群主权限转让
     * @param userId 新群主的用户id
     * @param groupId 群组id
     * @param optUserId 当前群主的用户id
     * @return
     */
    @ApiOperation("群主权限转让")
    @RequestMapping(value = "/leaderTransfer",method = RequestMethod.POST)
    public ResultUtil<Object> leaderTransfer(@RequestParam("userId") int userId,
                                             @RequestParam("groupId") int groupId,
                                             @RequestParam("optUserId") int optUserId){

        return adminService.leaderTransferService(userId, groupId, optUserId);
    }
}
