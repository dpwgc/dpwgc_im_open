package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.UploadService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

@Api("文件上传相关api")
@CrossOrigin
@RestController
@RequestMapping(value = "/Upload")
public class UploadController {

    @Autowired
    UploadService uploadService;

    /**
     * 上传文件
     * @param file 文件流
     * @param type 文件类型说明
     * @param session
     * @return
     */
    @ApiOperation("上传文件")
    @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
    public ResultUtil<Object> uploadFile(@RequestParam(value = "file") MultipartFile file,
                                         @RequestParam(value = "type") String type , HttpSession session){
        if (file==null){
            ResultUtil<Object> result = new ResultUtil<>();
            result.setCode(100);
            result.setMsg("文件内容为空");
            return result;
        }
        return uploadService.uploadFileService(file,type,session);
    }
}
