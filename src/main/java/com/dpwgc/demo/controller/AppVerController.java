package com.dpwgc.demo.controller;

import com.dpwgc.demo.service.AppVerService;
import com.dpwgc.demo.utils.ResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api("APP版本更新相关api")
@RestController
@RequestMapping(value = "/AppVer")
public class AppVerController {

    @Autowired
    AppVerService appVerService;

    /**
     * 获取APP最新版本信息
     * @return
     */
    @ApiOperation("获取APP最新版本信息")
    @RequestMapping(value = "/queryNewAppVer",method = RequestMethod.POST)
    public ResultUtil<Object> queryNewAppVer(){

        return appVerService.queryNewAppVerService();
    }

    /**
     * 获取APP全部版本信息
     * @return
     */
    @ApiOperation("获取APP全部版本信息")
    @RequestMapping(value = "/queryAllAppVer",method = RequestMethod.POST)
    public ResultUtil<Object> queryAllAppVer(){

        return appVerService.queryAllAppVerService();
    }
}
