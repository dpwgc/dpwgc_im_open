# [dpwgc_im]基于Spring Boot整合WebSocket的IM系统后端


<a name="overview"></a>
## 概览
项目相关接口描述


### 版本信息
*版本* : 1.0.0


### 联系方式
*名字* : dpwgc


### URI scheme
*域名* : localhost:8092  
*基础路径* : /


### 标签

* admin-controller : Admin Controller
* app-ver-controller : App Ver Controller
* group-controller : Group Controller
* message-controller : Message Controller
* test-controller : Test Controller
* upload-controller : Upload Controller
* user-controller : User Controller



