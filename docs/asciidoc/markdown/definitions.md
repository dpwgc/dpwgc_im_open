
<a name="definitions"></a>
## 定义

<a name="3fe9a946ea3238342a6fc5dc91782754"></a>
### ResultUtil«object»

|名称|说明|类型|
|---|---|---|
|**code**  <br>*可选*|**样例** : `0`|integer (int32)|
|**data**  <br>*可选*|**样例** : `"object"`|object|
|**msg**  <br>*可选*|**样例** : `"string"`|string|



