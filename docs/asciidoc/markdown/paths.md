
<a name="paths"></a>
## 资源

<a name="admin-controller_resource"></a>
### Admin-controller
Admin Controller


<a name="checkadminlvusingpost"></a>
#### 判断用户是否是管理员 & 检查管理员等级
```
POST /Admin/checkAdminLv
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/checkAdminLv?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="deleteadminusingpost"></a>
#### 群主撤销管理员权限
```
POST /Admin/deleteAdmin
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/deleteAdmin?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="insertadminusingpost"></a>
#### 新建管理员
```
POST /Admin/insertAdmin
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**lv**  <br>*必填*|lv|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/insertAdmin?groupId=0&lv=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="leadertransferusingpost"></a>
#### 群主权限转让
```
POST /Admin/leaderTransfer
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**optUserId**  <br>*必填*|optUserId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/leaderTransfer?groupId=0&optUserId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="queryuserapplybygroupidusingpost"></a>
#### 根据群组id获取申请加入群组的用户列表
```
POST /Admin/queryUserApplyByGroupId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/queryUserApplyByGroupId?groupId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="userexitgroupusingpost"></a>
#### 用户退出群组
```
POST /Admin/userExitGroup
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/userExitGroup?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="userjoingroupusingpost"></a>
#### 批准用户加入群组
```
POST /Admin/userJoinGroup
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Admin/userJoinGroup?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="app-ver-controller_resource"></a>
### App-ver-controller
App Ver Controller


<a name="queryallappverusingpost"></a>
#### 获取APP全部版本信息
```
POST /AppVer/queryAllAppVer
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/AppVer/queryAllAppVer
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="querynewappverusingpost"></a>
#### 获取APP最新版本信息
```
POST /AppVer/queryNewAppVer
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/AppVer/queryNewAppVer
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="group-controller_resource"></a>
### Group-controller
Group Controller


<a name="insertgroupusingpost"></a>
#### 新建群组
```
POST /Group/insertGroup
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupImg**  <br>*必填*|groupImg|string|
|**Query**|**groupName**  <br>*必填*|groupName|string|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/insertGroup?groupImg=string&groupName=string&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="queryadminbygroupidusingpost"></a>
#### 根据群组id获取群组管理员列表
```
POST /Group/queryAdminByGroupId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/queryAdminByGroupId?groupId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="querygroupbykeywordusingpost"></a>
#### 根据关键字搜索群组
```
POST /Group/queryGroupByKeyword
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**endPage**  <br>*必填*|endPage|integer (int32)|
|**Query**|**keyword**  <br>*必填*|keyword|string|
|**Query**|**startPage**  <br>*必填*|startPage|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/queryGroupByKeyword?endPage=0&keyword=string&startPage=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="querygroupbyuseridusingpost"></a>
#### 根据用户id获取用户群组列表
```
POST /Group/queryGroupByUserId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/queryGroupByUserId?userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="querysuperadminbygroupidusingpost"></a>
#### 根据群组id获取群组群主
```
POST /Group/querySuperAdminByGroupId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/querySuperAdminByGroupId?groupId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="queryuserbygroupidusingpost"></a>
#### 根据群组id获取群组用户列表
```
POST /Group/queryUserByGroupId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Group/queryUserByGroupId?groupId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="message-controller_resource"></a>
### Message-controller
Message Controller


<a name="deletemessageusingpost"></a>
#### 删除消息
```
POST /Message/deleteMessage
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*必填*|id|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Message/deleteMessage?id=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="insertmessageusingpost"></a>
#### 插入消息
```
POST /Message/insertMessage
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**messageData**  <br>*必填*|messageData|string|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Message/insertMessage?groupId=0&messageData=string&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="querymessagebygroupidusingpost"></a>
#### 根据群组id返回该群组的最新消息列表
```
POST /Message/queryMessageByGroupId
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**endPage**  <br>*必填*|endPage|integer (int32)|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**startPage**  <br>*必填*|startPage|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Message/queryMessageByGroupId?endPage=0&groupId=0&startPage=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="test-controller_resource"></a>
### Test-controller
Test Controller


<a name="testusingpost"></a>
#### test
```
POST /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusingget"></a>
#### test
```
GET /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusingput"></a>
#### test
```
PUT /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusingdelete"></a>
#### test
```
DELETE /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusingpatch"></a>
#### test
```
PATCH /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusinghead"></a>
#### test
```
HEAD /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="testusingoptions"></a>
#### test
```
OPTIONS /Test/test
```


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|无内容|
|**204**|No Content|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Test/test
```


<a name="upload-controller_resource"></a>
### Upload-controller
Upload Controller


<a name="uploadfileusingpost"></a>
#### 上传文件
```
POST /Upload/uploadFile
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**creationTime**  <br>*可选*||integer (int64)|
|**Query**|**id**  <br>*可选*||string|
|**Query**|**lastAccessedTime**  <br>*可选*||integer (int64)|
|**Query**|**maxInactiveInterval**  <br>*可选*||integer (int32)|
|**Query**|**new**  <br>*可选*||boolean|
|**Query**|**servletContext.classLoader**  <br>*可选*||ref|
|**Query**|**servletContext.contextPath**  <br>*可选*||string|
|**Query**|**servletContext.defaultSessionTrackingModes**  <br>*可选*||< enum (COOKIE, URL, SSL) > array(multi)|
|**Query**|**servletContext.effectiveMajorVersion**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.effectiveMinorVersion**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.effectiveSessionTrackingModes**  <br>*可选*||< enum (COOKIE, URL, SSL) > array(multi)|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].buffer**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].defaultContentType**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].deferredSyntaxAllowedAsLiteral**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].elIgnored**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].errorOnUndeclaredNamespace**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].includeCodas**  <br>*可选*||< string > array(multi)|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].includePreludes**  <br>*可选*||< string > array(multi)|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].isXml**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].pageEncoding**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].scriptingInvalid**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].trimDirectiveWhitespaces**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.jspPropertyGroups[0].urlPatterns**  <br>*可选*||< string > array(multi)|
|**Query**|**servletContext.jspConfigDescriptor.taglibs[0].taglibLocation**  <br>*可选*||string|
|**Query**|**servletContext.jspConfigDescriptor.taglibs[0].taglibURI**  <br>*可选*||string|
|**Query**|**servletContext.majorVersion**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.minorVersion**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.requestCharacterEncoding**  <br>*可选*||string|
|**Query**|**servletContext.responseCharacterEncoding**  <br>*可选*||string|
|**Query**|**servletContext.serverInfo**  <br>*可选*||string|
|**Query**|**servletContext.servletContextName**  <br>*可选*||string|
|**Query**|**servletContext.sessionCookieConfig.comment**  <br>*可选*||string|
|**Query**|**servletContext.sessionCookieConfig.domain**  <br>*可选*||string|
|**Query**|**servletContext.sessionCookieConfig.httpOnly**  <br>*可选*||boolean|
|**Query**|**servletContext.sessionCookieConfig.maxAge**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.sessionCookieConfig.name**  <br>*可选*||string|
|**Query**|**servletContext.sessionCookieConfig.path**  <br>*可选*||string|
|**Query**|**servletContext.sessionCookieConfig.secure**  <br>*可选*||boolean|
|**Query**|**servletContext.sessionTimeout**  <br>*可选*||integer (int32)|
|**Query**|**servletContext.virtualServerName**  <br>*可选*||string|
|**Query**|**type**  <br>*必填*|type|string|
|**Query**|**valueNames**  <br>*可选*||< string > array(multi)|
|**FormData**|**file**  <br>*必填*|file|file|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `multipart/form-data`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/Upload/uploadFile?type=string
```


###### 请求 formData
```json
"file"
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="user-controller_resource"></a>
### User-controller
User Controller


<a name="insertuserusingpost"></a>
#### 注册
```
POST /User/insertUser
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**headImg**  <br>*必填*|headImg|string|
|**Query**|**nickname**  <br>*必填*|nickname|string|
|**Query**|**password**  <br>*必填*|password|string|
|**Query**|**phone**  <br>*必填*|phone|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/User/insertUser?headImg=string&nickname=string&password=string&phone=string
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="loginusingpost"></a>
#### 登录
```
POST /User/login
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*必填*|id|integer (int32)|
|**Query**|**password**  <br>*必填*|password|string|
|**Query**|**phone**  <br>*必填*|phone|string|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/User/login?id=0&password=string&phone=string
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="queryuserbyidusingpost"></a>
#### 根据用户id返回用户信息
```
POST /User/queryUserById
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**id**  <br>*必填*|id|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/User/queryUserById?id=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="userapplygroupusingpost"></a>
#### 用户申请加入群组
```
POST /User/userApplyGroup
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/User/userApplyGroup?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```


<a name="userexitgroupusingpost_1"></a>
#### 用户退出群组
```
POST /User/userExitGroup
```


##### 参数

|类型|名称|说明|类型|
|---|---|---|---|
|**Query**|**groupId**  <br>*必填*|groupId|integer (int32)|
|**Query**|**userId**  <br>*必填*|userId|integer (int32)|


##### 响应

|HTTP代码|说明|类型|
|---|---|---|
|**200**|OK|[ResultUtil«object»](#3fe9a946ea3238342a6fc5dc91782754)|
|**201**|Created|无内容|
|**401**|Unauthorized|无内容|
|**403**|Forbidden|无内容|
|**404**|Not Found|无内容|


##### 消耗

* `application/json`


##### 生成

* `\*/*`


##### HTTP请求示例

###### 请求 path
```
/User/userExitGroup?groupId=0&userId=0
```


##### HTTP响应示例

###### 响应 200
```json
{
  "code" : 0,
  "data" : "object",
  "msg" : "string"
}
```



