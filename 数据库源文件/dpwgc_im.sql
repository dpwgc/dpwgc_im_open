/*
Navicat MySQL Data Transfer

Source Server         : SCP_AIAD
Source Server Version : 80017
Source Host           : localhost:3306
Source Database       : dpwgc_im

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2021-12-28 21:46:37
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for im_admin
-- ----------------------------
DROP TABLE IF EXISTS `im_admin`;
CREATE TABLE `im_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `group_id` int(11) DEFAULT NULL COMMENT '群组id',
  `lv` int(11) DEFAULT NULL COMMENT '管理员等级（1群主 2普通管理员）',
  `status` int(11) DEFAULT NULL COMMENT '状态 1正常 2封禁 3删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for im_app_ver
-- ----------------------------
DROP TABLE IF EXISTS `im_app_ver`;
CREATE TABLE `im_app_ver` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '版本编号',
  `app_ver` varchar(255) DEFAULT NULL COMMENT '版本号',
  `ver_content` varchar(255) DEFAULT NULL COMMENT '版本更新内容',
  `url` varchar(255) DEFAULT NULL COMMENT '版本更新url',
  `create_time` datetime DEFAULT NULL COMMENT '该版本创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for im_group
-- ----------------------------
DROP TABLE IF EXISTS `im_group`;
CREATE TABLE `im_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '群组id',
  `group_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '群组名称',
  `group_img` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '群组图片',
  `group_text` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '群组最新消息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(11) DEFAULT NULL COMMENT '状态 1正常 2封禁 3删除',
  `chat_num` int(11) DEFAULT NULL COMMENT '群聊消息总数',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for im_message
-- ----------------------------
DROP TABLE IF EXISTS `im_message`;
CREATE TABLE `im_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '消息id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `group_id` int(11) DEFAULT NULL COMMENT '群组id',
  `message_data` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '消息内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `status` int(11) DEFAULT NULL COMMENT '状态 1正常 2撤回 3删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11223 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for im_notice
-- ----------------------------
DROP TABLE IF EXISTS `im_notice`;
CREATE TABLE `im_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `group_id` int(11) DEFAULT NULL COMMENT '群组id',
  `notice_text` varchar(255) DEFAULT NULL COMMENT '通知文本',
  `notice_type` int(11) DEFAULT NULL COMMENT '通知类型：1系统通知 2群组通知',
  `create_time` datetime DEFAULT NULL COMMENT '通知时间',
  `status` int(11) DEFAULT NULL COMMENT '通知状态：0删除 1正常',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Table structure for im_user
-- ----------------------------
DROP TABLE IF EXISTS `im_user`;
CREATE TABLE `im_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话号码',
  `nickname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户昵称',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '用户密码',
  `head_img` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '用户头像',
  `status` int(11) DEFAULT NULL COMMENT '用户状态 1正常 2封禁 3删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for im_user_group
-- ----------------------------
DROP TABLE IF EXISTS `im_user_group`;
CREATE TABLE `im_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `group_id` int(11) DEFAULT NULL COMMENT '群组id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `status` int(11) DEFAULT NULL COMMENT '状态：1已加群 2申请加群中 3退出群',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `look_num` int(11) DEFAULT NULL COMMENT '已读消息数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
