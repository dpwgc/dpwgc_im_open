# dpwgc_im.即时通讯系统 `毕业设计`

* <https://gitee.com/dpwgc/dpwgc_im_open> `项目后端(Java版本)`
* <https://gitee.com/dpwgc/dpwgc_im_go_ws> `WebSocket消息推送模块(Golang版本)`
* <https://gitee.com/dpwgc/dpwgc_im_web> `项目前端(uni-app)`

***

## dpwgc_im.基于Spring Boot整合WebSocket的IM系统后端(Java版本)

`SpringBoot` `WebSocket` `MySQL` `Redis` `MyBatis-Plus`

***

### 接口文档

   使用Swagger作接口文档，项目启动后访问http://{部署地址}:{部署端口}/swagger-ui.html
   
***

### 项目结构
* main
   * java/com.dpwgc.demo
      * config `配置类`
      * controller `控制器层`
      * interceptor `登录拦截器`
      * mapper `模板映射层`
      * model `模板类`
      * server`服务层(WebSocket消息推送模块)`
      * service `服务层(常规业务请求处理)`
      * utils `工具类`
      * DemoApplication  `启动类`
   * resources
      * mapper `xml配置文件`
      * static `静态文件存放`
      * application.properties `SpringBoot配置文件`

***

### 功能模块

* Admin*** `群组管理员模块`

* AppVer*** `APP版本控制模块`

* Group*** `群组模块`

* Message*** `群聊消息模块`

* Notice*** `群组通知模块`

* Upload*** `文件上传模块（使用腾讯云对象存储，需要在application.properties配置）`

* User*** `用户模块`

* ***LinkServer `WebSocket消息推送模块`

***

### WebSocket消息推送

* ChatLinkServer `群聊内部消息推送模块`

* IndexLinkServer `用户主页群组消息推送模块`

WebSocket消息推送模块(Golang版本)
<https://gitee.com/dpwgc/dpwgc_im_go_ws>

***

### 使用说明
   填写application.properties内的数据库配置和腾讯云对象存储配置，打包成jar包部署
   